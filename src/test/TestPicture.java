package test;

import main.Landscape;
import main.Picture;
import main.Portrait;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestPicture {
    @Test
    public void testPortrait(){
        Picture picture = new Portrait();
        assertEquals("Портрет,Нарисовали человека,Раскрасили", picture.draw());
    }

    @Test
    public void testLandscape(){
        Picture picture = new Landscape();
        assertEquals("Пейзаж,Нарисовали природу,Раскрасили", picture.draw());
    }
}
