package main;

public class Portrait extends Picture{
    @Override
    String chooseGenre() {
        return "Портрет";
    }

    @Override
    String makeSketch() {
        return "Нарисовали человека";
    }

    @Override
    String paint() {
        return "Раскрасили";
    }
}
