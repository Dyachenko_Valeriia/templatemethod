package main;

public abstract class Picture {

    abstract String chooseGenre();
    abstract String makeSketch();
    abstract String paint();

    public final String draw() {
        return chooseGenre() +","+ makeSketch() +","+ paint();
    }
}