package main;

public class Landscape extends Picture{ //Пейзаж
    @Override
    String chooseGenre() {
        return "Пейзаж";
    }

    @Override
    String makeSketch() {
        return "Нарисовали природу";
    }

    @Override
    String paint() {
        return "Раскрасили";
    }

}
